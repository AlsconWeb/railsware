<?php
/**
 * Header template.
 *
 * @package iwpdev/railsware
 */

use Railsware\Main;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<section>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php the_custom_logo(); ?>

					<a class="button" href="#">
						<?php esc_html_e( 'Hire Us', Main::RS_DOMAIN_NAME ); ?>
					</a>
					<div class="burger-menu">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'top_menu',
							'container'      => '',
							'menu_class'     => 'menu',
							'echo'           => true,
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						]
					);
					?>
				</div>
			</div>
		</div>
	</header>
