<?php
/**
 * Main theme file.
 *
 * @package iwpdev/railsware
 */

use Railsware\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();
