<?php
/**
 * Main class theme railsware.
 *
 * @package iwpdev/railsware
 */

namespace Railsware;

/**
 * Railswere class file.
 */
class Main {

	/**
	 * Theme version.
	 */
	public const RS_VERSION = '1.0.0a';

	/**
	 * Domain name from language.
	 */
	public const RS_DOMAIN_NAME = 'railsware';

	/**
	 * Railswere construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo', [ 'unlink-homepage-logo' => true ] );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_menu' => __( 'Menu in header', self::RS_DOMAIN_NAME ),
			]
		);
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {

		$home  = esc_url( get_bloginfo( 'url' ) );
		$class = 'logo';
		if ( has_custom_logo() ) {
			$logo    = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'logo',
					'itemprop' => 'logo',
				]
			);
			$content = $logo;

			$content .= '<span class="sr-only">' . get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' ) . '</span>';

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				$home,
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Add script adn style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		$url = get_stylesheet_directory_uri();

		wp_enqueue_style( 'rs_style', $url . '/style.css', '', self::RS_VERSION );
		wp_enqueue_style( 'rs_main', $url . '/assets/css/main.css', '', self::RS_VERSION );
		wp_enqueue_style( 'rs_google_font', '//fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap', '', self::RS_VERSION );

		wp_enqueue_script( 'rs_build', $url . '/assets/js/build.js', [ 'jquery' ], self::RS_VERSION, true );

	}

}