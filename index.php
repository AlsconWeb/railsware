<?php
/**
 * Index template.
 *
 * @package iwpdev/railsware
 */

get_header();
?>
	<div class="main-content">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
			}
		}
		?>
	</div>
<?php
get_footer();
